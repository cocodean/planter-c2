/////////////////////////////////////////////////////////////////////
/// @File SoilMoistureSensor.cpp
/// @brief 
/////////////////////////////////////////////////////////////////////

#include "SoilMoistureSensor.h"

#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/mman.h>

#include <thread>

#define DEBUG_READ

//---------------------------------------------------------------------------//

SoilMoistureSensor::SoilMoistureSensor()
{
	if (false == initializeSensor())
	{
		std::string errorMsg = "ERROR: SoilMoistureSensor - failed to initialize gpio pins for soil moisture sensor.\n";
		std::cerr << errorMsg;

		throw std::runtime_error(errorMsg);
	}

	std::cout << "INFO: SoilMoistureSensor - ready for use...\n";
}

//---------------------------------------------------------------------------//

float SoilMoistureSensor::readSensor()
{
	float moisutreValue = 0;

	try
	{
		// first set power line high to enable the read pin
		gpiod_line_set_value(mPowerGpioLine, 1);
		
#ifdef DEBUG_READ
		std::this_thread::sleep_for(std::chrono::milliseconds(5000));		// wait 5 seconds
#endif // DEBUG_READ

		// read the read pin
		moisutreValue = static_cast<float>(gpiod_line_get_value(mReadGpioLine));

		// turn on the power line, bad for read line staying on
		gpiod_line_set_value(mPowerGpioLine, 0);
	}
	catch (const std::exception &e)
	{
		std::cerr << e.what() << std::endl;
		moisutreValue = -512.00;
	}

	return moisutreValue;
}

//---------------------------------------------------------------------------//

SoilMoistureSensor::~SoilMoistureSensor()
{
	closeGpioPins();
}

//---------------------------------------------------------------------------//

bool SoilMoistureSensor::initializeSensor()
{
	bool initResult = true;

	try
	{
		// open the gpiochip
		mGpioChip = gpiod_chip_open_by_name(DEFAULT_GPIOCHIP_NAME.c_str());

		// check opened chip correctly
		if (!mGpioChip)
		{
			initResult = false;

			std::string errorMsg = "ERROR: SoilMoistureSensor - failed to open the gpio chip	" + DEFAULT_GPIOCHIP_NAME;
			std::cerr << errorMsg << std::endl;
		}
		else
		{
			std::cout << "INFO: SoilMoistureSensor - successfully opened the gpio chip " << DEFAULT_GPIOCHIP_NAME << std::endl;
			
			// open power gpio line, output
			initResult = initializeGpioPowerPin();

			// open read gpio line, input
			initResult = initResult && initializeGpioReadPin();
		}
	}
	catch (const std::exception &e)
	{
		initResult = false;
		std::cerr << e.what() << std::endl;
	}

	return initResult;
}

//---------------------------------------------------------------------------//

bool SoilMoistureSensor::initializeGpioPowerPin()
{
	bool initResult = true;

	try
	{
		// open gpio line for power pin
		mPowerGpioLine = gpiod_chip_get_line(mGpioChip, DEFAULT_POWER_GPIO_PIN);

		// check line opened correctly
		if (!mPowerGpioLine)
		{
			std::string errorMsg = "ERROR: SoilMoistureSensor - failed to open the power gpio line	" 
				+ std::to_string(DEFAULT_POWER_GPIO_PIN);
			std::cerr << errorMsg << std::endl;
		}
		else
		{
			std::cout << "INFO: SoilMoistureSensor - successfully opened the power line.\n";

			// set gpio line as output
			mPowerLineConsumerName = "PowerConsumer" + std::to_string(DEFAULT_POWER_GPIO_PIN);
			int setOutputResult = gpiod_line_request_output(mPowerGpioLine, mPowerLineConsumerName.c_str(), DEFAULT_START_VALUE);

			// check output set was successful
			if (setOutputResult < 0)
			{
				std::string errorMsg = "ERROR: SoilMoistureSensor - failed to set as output for power gpio line	"
					+ std::to_string(DEFAULT_POWER_GPIO_PIN);
				std::cerr << errorMsg << std::endl;
			}
			else
			{
				std::cout << "INFO: SoilMoistureSensor - successfully set power line as output.\n";
			}
		}
	}
	catch (const std::exception &e)
	{
		std::cerr << e.what() << std::endl;
		initResult = false;
	}

   return initResult;
}

//---------------------------------------------------------------------------//

bool SoilMoistureSensor::initializeGpioReadPin()
{
	bool initResult = true;

	try
	{
		// open gpio read line
		mReadGpioLine = gpiod_chip_get_line(mGpioChip, DEFAULT_READ_GPIO_PIN);

		// check read line opened correctly
		if (!mReadGpioLine)
		{
			std::string errorMsg = "ERROR: SoilMoistureSensor - failed to open the read gpio line	"
				+ std::to_string(DEFAULT_READ_GPIO_PIN);
			std::cerr << errorMsg << std::endl;
		}
		else
		{
			std::cout << "INFO: SoilMoistureSensor - successfully opened the read line.\n";

			// set gpio line as input
			mPowerLineConsumerName = "ReadConsumer" + std::to_string(DEFAULT_READ_GPIO_PIN);
			int setInputResult = gpiod_line_request_input(mReadGpioLine, mReadLineConsumerName.c_str());

			// check output set was successful
			if (setInputResult < 0)
			{
				std::string errorMsg = "ERROR: SoilMoistureSensor - failed to set as input for gpio read line	"
					+ std::to_string(DEFAULT_READ_GPIO_PIN);
				std::cerr << errorMsg << std::endl;
			}
			else
			{
				std::cout << "INFO: SoilMoistureSensor - successfully set read line as input.\n";
			}
		}
	}
	catch (const std::exception &e)
	{
		std::cerr << e.what() << std::endl;
		initResult = false;
	}

	return initResult;
}

//---------------------------------------------------------------------------//

void SoilMoistureSensor::closeGpioPins()
{
	// release the power line first
	if (mPowerGpioLine)
	{
		gpiod_line_release(mPowerGpioLine);
	}

	// release the read line
	if (mReadGpioLine)
	{
		gpiod_line_release(mReadGpioLine);
	}

	// close the chip
	if (mGpioChip)
	{
		gpiod_chip_close(mGpioChip);
	}
}
