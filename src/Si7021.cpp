/////////////////////////////////////////////////////////////////////
/// @File Si7021.cpp
/// @brief Defines the Si7021 implementation
/////////////////////////////////////////////////////////////////////

#include "Si7021.h"
#include "Si7021_Registers.h"

#include <iostream>
#include <string.h>              ///< For memcpy
#include <stdio.h>               ///< For memcpy
#include <limits>                ///< For the float min/max
#include <thread>                ///< For the sleep_for function

#include <linux/i2c-dev.h>	      ///< For i2c bus access on Raspberry Pi
#include <fcntl.h>		         ///< For i2c bus handle init, O_RDWR
#include <unistd.h>		         ///< For i2c bus handle init,  open()
#include <sys/ioctl.h>		      ///< Joins the sensor to the i2c bus
#include <errno.h>

#define DEBUG_READ_TEMPERATURE
#define DEBUG_READ_RH

//---------------------------------------------------------------------------//

Si7021::Si7021()
{
   mAddress = SI7021_I2C_SLAVE_ADDRESS;
   mIsConnectedToBus = false;

   if (false == initializeBusConnection())
   {
      // failed to initialize i2c bus connection
      std::string errorMsg = "Error: Si7021 - failed to initialize the i2c bus connection.\n";
      std::cerr << errorMsg;

      throw std::runtime_error(errorMsg);
   }
}

//---------------------------------------------------------------------------//

Si7021::Si7021(const uint8_t address)
{
   mAddress = address;
   mIsConnectedToBus = false;

   if (false == initializeBusConnection())
   {
      // failed to initialize i2c bus connection
      std::string errorMsg = "Error: Si7021 - failed to initialize the i2c bus connection.\n";
      std::cerr << errorMsg;

      throw std::runtime_error(errorMsg);
   }
}

//---------------------------------------------------------------------------//

Si7021::~Si7021()
{
   if (mIsConnectedToBus)
   {
      closeBusConnection();
   }
}

//---------------------------------------------------------------------------//

float Si7021::readTemperature(bool masterHold)
{
   float temperatureResult = 0;

   if (!mIsConnectedToBus)
   {
      std::cout << "WARNING: Si7021 - need to connect to i2c bus before reading temperature.\n";
      temperatureResult = std::numeric_limits<float>::min();
      return temperatureResult;
   }

   try
   {
      char bufferWrite;
      bufferWrite = (masterHold) ? MEASURE_TEMPERATURE_HOLD_MASTER_MODE : MEASURE_TEMPERATURE_NO_HOLD_MASTER_MODE;

      // ask slave for data at address
      if (write(mI2cHandle, &bufferWrite, sizeof(bufferWrite) != sizeof(bufferWrite)))
      {
         std::cerr << "ERROR: Si7021 - error writing to temperature register.\n";

         // check errno to see what went wrong
         std::cerr << errno << std::endl;

         temperatureResult = std::numeric_limits<float>::max();
      }
      else
      {
         // wait before reading the i2c bus
         std::this_thread::sleep_for(std::chrono::milliseconds(I2C_WRITE_READ_WAIT_MS));

         unsigned char readBuffer[TEMPERATURE_SIZE_BYTES];

         // read out the data sent back
         if (read(mI2cHandle, readBuffer, TEMPERATURE_SIZE_BYTES) != TEMPERATURE_SIZE_BYTES)
         {
            std::cerr << "ERROR: Si7021 - error reading the temperature data from i2c bus.\n";

            // check errno to see what went wrong
            std::cerr << errno << std::endl;

            temperatureResult = std::numeric_limits<float>::max();
         }
         else
         {
            temperatureResult = convertToTemperature_F(readBuffer);
         }
      }
   }
   catch (const std::exception &e)
   {
      std::cerr << e.what() << std::endl;
      temperatureResult = std::numeric_limits<float>::max();
   }

   return temperatureResult;
}

//---------------------------------------------------------------------------//

float Si7021::readTemperatureRH()
{
   float temperatureResult = 0;

   if (!mIsConnectedToBus)
   {
      std::cout << "WARNING: Si7021 - need to connect to i2c bus before reading temperature.\n";
      temperatureResult = std::numeric_limits<float>::min();
      return temperatureResult;
   }

   try
   {
      char bufferWrite;
      bufferWrite = MEASURE_TEMPERATURE_FROM_LAST_RH_MEASUREMENT;

      // ask slave for data at address
      if (write(mI2cHandle, &bufferWrite, sizeof(bufferWrite) != sizeof(bufferWrite)))
      {
         std::cerr << "ERROR: Si7021 - error writing to temperature register.\n";

         // check errno to see what went wrong
         std::cerr << errno << std::endl;

         temperatureResult = std::numeric_limits<float>::max();
      }
      else
      {
         // wait before reading the i2c bus
         std::this_thread::sleep_for(std::chrono::milliseconds(I2C_WRITE_READ_WAIT_MS));

         unsigned char readBuffer[TEMPERATURE_SIZE_BYTES];

         // read out the data sent back
         if (read(mI2cHandle, readBuffer, TEMPERATURE_SIZE_BYTES) != TEMPERATURE_SIZE_BYTES)
         {
            std::cerr << "ERROR: Si7021 - error reading the temperature data from i2c bus.\n";

            // check errno to see what went wrong
            std::cerr << errno << std::endl;

            temperatureResult = std::numeric_limits<float>::max();
         }
         else
         {
            temperatureResult = convertToTemperature_F(readBuffer);
         }
      }
   }
   catch (const std::exception &e)
   {
      std::cerr << e.what() << std::endl;
      temperatureResult = std::numeric_limits<float>::max();
   }

   return temperatureResult;
}

//---------------------------------------------------------------------------//

float Si7021::readRelativeHumidity(bool masterHold)
{
   float relativeHumidityResult = 0;

   if (!mIsConnectedToBus)
   {
      std::cout << "WARNING: Si7021 - need to connect to i2c bus before reading relative humidity.\n";
      relativeHumidityResult = std::numeric_limits<float>::min();
      return relativeHumidityResult;
   }

   try
   {
      // ask slave for data at address

      char bufferWrite;
      bufferWrite = (masterHold) ? MEASURE_RH_HOLD_MASTER_MODE : MEASURE_RH_NO_HOLD_MASTER_MODE;

      if (write(mI2cHandle, &bufferWrite, sizeof(bufferWrite) != sizeof(bufferWrite)))
      {
         std::cerr << "ERROR: Si7021 - error writing to relative humidity register.\n";

         // check errno to see what went wrong
         std::cerr << errno << std::endl;

         relativeHumidityResult = std::numeric_limits<float>::max();
      }
      else
      {
         // wait before reading the i2c bus
         std::this_thread::sleep_for(std::chrono::milliseconds(I2C_WRITE_READ_WAIT_MS));

         unsigned char readBuffer[RELATIVE_HUMIDITY_SIZE_BYTES];

         // read out the data sent back
         if (read(mI2cHandle, readBuffer, RELATIVE_HUMIDITY_SIZE_BYTES) != RELATIVE_HUMIDITY_SIZE_BYTES)
         {
            std::cerr << "ERROR: Si7021 - error reading the relative humidity data from i2c bus.\n";

            // check errno to see what went wrong
            std::cerr << errno << std::endl;

            relativeHumidityResult = std::numeric_limits<float>::max();
         }
         else
         {
            relativeHumidityResult = convertToRelativeHumidity(readBuffer);
         }
      }
   }
   catch (const std::exception &e)
   {
      std::cerr << e.what() << std::endl;
      relativeHumidityResult = std::numeric_limits<float>::max();
   }

   return relativeHumidityResult;
}

//---------------------------------------------------------------------------//

bool Si7021::initializeBusConnection()
{
   bool initResult = true;

   if (mIsConnectedToBus)
   {
      std::cout << "INFO: Si7021 - already connected to i2c bus fd.\n";
      return initResult;
   }

   try
   {
      mI2cHandle = open(DEFAULT_I2C_FILE_NAME.c_str(), O_RDWR);

      if (mI2cHandle < 0)
      {
         // error getting file descriptor for i2c bus
         initResult = false;

         std::cerr << "ERROR: Si7021 - failed to acquire the i2c bus fd.\n";

         // check errno to see what went wrong
         std::cerr << errno << std::endl;
      }
      else
      {
         std::cout << "INFO: Si7021 - successfully acquired the i2c bus fd.\n";

         // set the slave address as the target address
         if (ioctl(mI2cHandle, I2C_SLAVE, mAddress) < 0)
         {
            initResult = false;

            std::cerr << "ERROR: Si7021 - failed to acquire bus access and/or talk to slave during ioctl call.\n";

            // check errno to see what went wrong
            std::cerr << errno << std::endl;
         }
         else
         {
            mIsConnectedToBus = true;

            std::cout << "INFO: Si7021 - successfully set the i2c slave target address.\n";
         }
      }
   }
   catch (const std::exception &e)
   {
      std::cerr << e.what() << std::endl;
      initResult = false;
   }

   return initResult;
}

//---------------------------------------------------------------------------//

void Si7021::closeBusConnection()
{
   if (close(mI2cHandle) < 0)
   {
      std::cerr << "ERROR: Si7021 - failed to close the fd for the i2c bus.\n";

      // check errno to see what went wrong
      std::cerr << errno << std::endl;
   }
   else
   {
      mIsConnectedToBus = false;
   }
}

//---------------------------------------------------------------------------//

void Si7021::swapBytes(unsigned char dataBuffer[])
{
   try
   {
      unsigned char tempChar;

      tempChar = dataBuffer[0];
      dataBuffer[0] = dataBuffer[1];
      dataBuffer[1] = tempChar;
   }
   catch (const std::exception &e)
   {
      std::cerr << e.what() << std::endl;
   }
}

//---------------------------------------------------------------------------//

float Si7021::convertToTemperature_F(unsigned char dataBuffer[])
{
   float temperatureResult_F;

   try
   {
      swapBytes(dataBuffer);

#ifdef DEBUG_READ_TEMPERATURE
      std::cout << "DEBUG: Si7021 - the buffer from the temperature read is,\n";
      for (int j = 0; j < sizeof(dataBuffer); j++)
      {
         std::cout << std::to_string(j) << ":" << dataBuffer[j] << "\n";
      }
#endif // DEBUG_READ_TEMPERATURE

      uint16_t readRegVal;

      // copy over 16-bits of data read
      memcpy(&readRegVal, dataBuffer, sizeof(readRegVal));

#ifdef DEBUG_READ_TEMPERATURE
      std::cout << "DEBUG: Si7021 - the 16-bit register value for temperature is "
         << std::to_string(readRegVal) << std::endl;
#endif // DEBUG_READ_TEMPERATURE

      // 16-bit value to celsius temperature conversion
      double temperature_C = ((static_cast<double>(readRegVal) * 175.72) / 65536.0) - 46.85;

#ifdef DEBUG_READ_TEMPERATURE
      std::cout << "DEBUG: Si7021 - the temperature in Celsius is "
         << std::to_string(temperature_C) << std::endl;
#endif // DEBUG_READ_TEMPERATURE

      // celsius to farenheit conversion
      temperatureResult_F = static_cast<float>(temperature_C * 1.8 + 32.0);

#ifdef DEBUG_READ_TEMPERATURE
      std::cout << "DEBUG: Si7021 - the temperature in Farenheit is "
         << std::to_string(temperatureResult_F) << std::endl << std::endl;
#endif // DEBUG_READ_TEMPERATURE
   }
   catch (const std::exception &e)
   {
      std::cerr << e.what() << std::endl;
      temperatureResult_F = std::numeric_limits<float>::max();
   }

   return temperatureResult_F;
}

//---------------------------------------------------------------------------//

float Si7021::convertToRelativeHumidity(unsigned char dataBuffer[])
{
   float rhResult;

   try
   {
      swapBytes(dataBuffer);

#ifdef DEBUG_READ_RH
      std::cout << "DEBUG: Si7021 - the buffer from the relative humidity read is,\n";
      for (int j = 0; j < sizeof(dataBuffer); j++)
      {
         std::cout << std::to_string(j) << ":" << dataBuffer[j] << "\n";
      }
#endif // DEBUG_READ_RH

      uint16_t readRegVal;

      // copy over 16-bits of data read
      memcpy(&readRegVal, dataBuffer, sizeof(readRegVal));

#ifdef DEBUG_READ_RH
      std::cout << "DEBUG: Si7021 - the 16-bit register value for RH is "
         << std::to_string(readRegVal) << std::endl;
#endif // DEBUG_READ_RH

      // 16-bit to relative humidity conversion
      rhResult = static_cast<float>(((125.0 * static_cast<double>(readRegVal)) / 65536.0) - 6.0);

#ifdef DEBUG_READ_RH
      std::cout << "DEBUG: Si7021 - the relative humidity in %RH is "
         << std::to_string(rhResult) << std::endl << std::endl;
#endif // DEBUG_READ_RH
   }
   catch (const std::exception &e)
   {
      std::cerr << e.what() << std::endl;
      rhResult = std::numeric_limits<float>::max();
   }

   return rhResult;
}

//---------------------------------------------------------------------------//
