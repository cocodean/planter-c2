/////////////////////////////////////////////////////////////////////
/// @File main.cpp
/// @brief The entry point for the planter command and control application
/////////////////////////////////////////////////////////////////////

#include <wiringPi.h>
#include "Si7021.h"
#include "SoilMoistureSensor.h"
#include <thread>
#include <iostream>

// LED Pin - wiringPi pin 0 is BCM_GPIO 17.
// we have to use BCM numbering when initializing with wiringPiSetupSys
// when choosing a different pin number please use the BCM numbering, also
// update the Property Pages - Build Events - Remote Post-Build Event command
// which uses gpio export for setup for wiringPiSetupSys
#define	LED	17

int main(void)
{
	wiringPiSetupSys();

	pinMode(LED, OUTPUT);

	Si7021 myTempSensor;

	SoilMoistureSensor mySoilSensor;

	for (int i = 0; i < 10; i++)
	{
		float soilValue = mySoilSensor.readSensor();

		std::cout << "INFO: main - The read soil moisture sensor value is " << std::to_string(soilValue) << std::endl;

		std::this_thread::sleep_for(std::chrono::milliseconds(5000));
	}

	//// read temperature from measurement
	//for (int i = 0; i < 10; i++)
	//{
	//	float temperatureValue = myTempSensor.readTemperature();

	//	std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	//}

	//std::cout << "----------------------------------------------------------------------------" << std::endl;

	//// read relative humidity from measurement and temperature from RH measurement
	//for (int i = 0; i < 10; i++)
	//{
	//	float rhValue = myTempSensor.readRelativeHumidity();

	//	std::this_thread::sleep_for(std::chrono::milliseconds(1000));

	//	float temperatureValue = myTempSensor.readTemperatureRH();

	//	std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	//}

	while (true)
	{
		digitalWrite(LED, HIGH);  // On
		delay(500); // ms
		digitalWrite(LED, LOW);	  // Off
		delay(500);

		// read temperature data
	}

	return 0;
}