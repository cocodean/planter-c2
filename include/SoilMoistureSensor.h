#ifndef SOILMOISTURESENSOR_H
#define SOILMOISTURESENSOR_H

/////////////////////////////////////////////////////////////////////
/// @File SoilMoistureSensor.h
/// @brief The definition for the SoilMoistureSensor class
/////////////////////////////////////////////////////////////////////

#include <iostream>

#include <gpiod.h>

/////////////////////////////////////////////////////////////////////
/// @class SoilMoistureSensor
/// @brief Used to read the state of the soil
/// 
/// Defines an activation pin and read analog pin for the sensor. 
/////////////////////////////////////////////////////////////////////
class SoilMoistureSensor
{
public:
   // constructors
   SoilMoistureSensor();

   /// @brief Reads the soil moisture sensor
   /// @return The value in .....
   float readSensor();

   ~SoilMoistureSensor();

private:
   // Constants
   const std::string DEFAULT_GPIOCHIP_NAME = "gpiochip0";      ///< The gpio chip for line control
   const uint32_t DEFAULT_READ_GPIO_PIN = 20;                  ///< The default GPIO pin to read, pin 38
   const uint32_t DEFAULT_POWER_GPIO_PIN = 21;                 ///< The default GPIO pin to turn on, pin 40
   const uint32_t DEFAULT_START_VALUE = 0;                     ///< The default GPIO pin output, off

   // Variables
   struct gpiod_chip *mGpioChip;                               ///< The chip holding the gpio lines
   struct gpiod_line *mPowerGpioLine;                          ///< The gpio line to control
   struct gpiod_line *mReadGpioLine;                           ///< The gpio line to control
   std::string mPowerLineConsumerName;                         ///< The gpio power line consumer name
   std::string mReadLineConsumerName;                          ///< The gpio read line consumer name

   // Functions
   bool initializeSensor();
   bool initializeGpioPowerPin();
   bool initializeGpioReadPin();
   void closeGpioPins();
};

#endif // !SOILMOISTURESENSOR_H
