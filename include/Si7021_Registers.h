#ifndef SI7021_REGISTERS_H
#define SI7021_REGISTERS_H

/// /////////////////////////////////////////////////////////////////////
/// @File Si7021_Registers.h
/// @brief Defines the registers for the Si7021 I2C sensor device
/// 
/// Defines the I2C registers for the Si7021 Relative Humidity and
/// Temperature Sensor. The values defined in this file are from the 
/// Si7021 data sheet.
/////////////////////////////////////////////////////////////////////

//------------------------------ Command Table ------------------------------//

//------------------- Command Description ------------------- Command Code --//

static const uint8_t SI7021_I2C_SLAVE_ADDRESS 								   = 0x40;

//---------------------------------------------------------------------------//
// Hold Master Mode for clock stretching
// No Hold Master Mode for no clock stretching
static const uint8_t MEASURE_RH_HOLD_MASTER_MODE 						      = 0xE5;
static const uint8_t MEASURE_RH_NO_HOLD_MASTER_MODE 					      = 0xF5;

//---------------------------------------------------------------------------//
// Hold Master Mode for clock stretching
// No Hold Master Mode for no clock stretching
static const uint8_t MEASURE_TEMPERATURE_HOLD_MASTER_MODE 				   = 0xE3;
static const uint8_t MEASURE_TEMPERATURE_NO_HOLD_MASTER_MODE 			   = 0xF3;

//---------------------------------------------------------------------------//

static const uint8_t MEASURE_TEMPERATURE_FROM_LAST_RH_MEASUREMENT 		= 0xE0;

//---------------------------------------------------------------------------//

static const uint8_t RESET 											            = 0xFE;

//---------------------------------------------------------------------------//

static const uint8_t WRITE_RH_T_USER_REG1                               = 0xE6;
static const uint8_t READ_RH_T_USER_REG1 								         = 0xE7;

//---------------------------------------------------------------------------//

static const uint8_t WRITE_HEATER_CTRL_REG 							         = 0x51;
static const uint8_t READ_HEATER_CTRL_REG 								      = 0x11;

//---------------------------------------------------------------------------//
// Read Electronic Id 1st Byte 0xFA 0x0F
static const uint8_t READ_ELECTRONIC_ID_1B1 							         = 0xFA;
static const uint8_t READ_ELECTRONIC_ID_1B2 							         = 0x0F;

//---------------------------------------------------------------------------//
// Read Electronic Id 2nd Byte 0xFC 0xC9
static const uint8_t READ_ELECTRONIC_ID_2B1 							         = 0xFC;
static const uint8_t READ_ELECTRONIC_ID_2B2 							         = 0xC9;

//---------------------------------------------------------------------------//
// Read Firmware Revision
static const uint8_t READ_FIRMWARE_REVISION_1 							      = 0x84;
static const uint8_t READ_FIRMWARE_REVISION_2 							      = 0xB8;

//---------------------------------------------------------------------------//

#endif
