#ifndef SI7021_H
#define SI7021_H

/////////////////////////////////////////////////////////////////////
/// @File Si7021.h
/// @brief Defines the Si7021 sensor with i2c access on the Raspberry Pi
/////////////////////////////////////////////////////////////////////

#include <atomic>
#include <string>

/////////////////////////////////////////////////////////////////////
/// @class Si7021
/// @brief Opens the Si7021 sensor to allows temperature and reltaive humidity reads.
/////////////////////////////////////////////////////////////////////
class Si7021
{
public:
   /// @brief The default constructor
   Si7021();

   /// @brief Constructor to modify the default address within the header file
   /// @param[in] address The 7-bit address that identifies the i2c slave device
   Si7021(const uint8_t address);

   /// @brief The default destructor to close i2c file descriptor
   ~Si7021();

   /// @brief Reads the temperature data from temperature measurement
   /// @param[in] masterHold Whether to use the hold or no hold master register
   /// @return The converted temperature value in Farenheit
   /// @retval float::min Need to connect to the i2c bus
   /// @retval float::max Error during the transaction
   float readTemperature(bool masterHold = true);

   /// @brief Reads the temperature data from RH measurement
   /// @return The converted temperature value in Farenheit
   /// @retval float::min Need to connect to the i2c bus
   /// @retval float::max Error during the transaction
   float readTemperatureRH();

   /// @brief Reads the relative humidity data from measurement
   /// @param[in] masterHold Whether to use the hold or no hold master register
   /// @return The converted relative humidity value in %RH
   /// @retval float::min Need to connect to the i2c bus
   /// @retval float::max Error during the transaction
   float readRelativeHumidity(bool masterHold = true);

private:
   // Constants
   const std::string DEFAULT_I2C_FILE_NAME = "/dev/i2c-1";        ///< The path for the i2c bus
   const uint32_t I2C_WRITE_READ_WAIT_MS = 2;                     ///< The time to wait for write-read transactions
   static const uint32_t TEMPERATURE_SIZE_BYTES = 2;              ///< The size of the temperature data, 16-bit
   static const uint32_t RELATIVE_HUMIDITY_SIZE_BYTES = 2;        ///< The size of the relative humidty data, 16-bit

   // Variables
   uint8_t mAddress;		                                          ///< The 7-bit address of the i2c slave sensor
   int32_t mI2cHandle;                                            ///< The i2c file descriptor communication handle
   std::atomic_bool mIsConnectedToBus;                            ///< The flag to monitor when the slave has open the i2c bus

   /// @brief Initializes and sets the target slave address on the i2c bus
   /// @return Whether the initialization was a sucess
   /// @rerval true Successfully initialized the i2c bus
   /// @retval false Failed to initialize the i2c bus 
   bool initializeBusConnection();

   /// @brief Closes the file descriptor for the i2c bus
   void closeBusConnection();

   /// @brief Swaps the first and last bytes of an assumed char[2]
   /// @param[inout] dataBuffer The char buffer read from the i2c bus, assumed 16-bit
   void swapBytes(unsigned char dataBuffer[]);

   /// @brief Converts the read char buffer to temperature
   /// @param[in] dataBuffer The data read from the i2c bus
   /// @return The temperautre in farenheit
   /// @retval float::max Error reading the data
   float convertToTemperature_F(unsigned char dataBuffer[]);

   /// @brief Converts the read char buffer to relative humidity
   /// @param[in] dataBuffer The data read from the i2c bus
   /// @return The relative humidity in %RH
   /// @retval float::max Error reading the data
   float convertToRelativeHumidity(unsigned char dataBuffer[]);
};

#endif // !SI7021_H
